// ESP8266 Simple sniffer
// 2018 Carve Systems LLC
// Angel Suarez-B Martin

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include "sdk_structs.h"
#include "ieee80211_structs.h"
#include "string_utils.h"

#define LED_BUILTIN 16


extern "C"
{
  #include "user_interface.h"
}


int snotBlinkInterval = 2000;
unsigned long snotBlinkMillis = 0;
int snotLEDState = HIGH;


// According to the SDK documentation, the packet type can be inferred from the
// size of the buffer. We are ignoring this information and parsing the type-subtype
// from the packet header itself. Still, this is here for reference.
wifi_promiscuous_pkt_type_t packet_type_parser(uint16_t len)
{
  	switch(len)
    {
      // If only rx_ctrl is returned, this is an unsupported packet
      case sizeof(wifi_pkt_rx_ctrl_t):
      return WIFI_PKT_MISC;

      // Management packet
      case sizeof(wifi_pkt_mgmt_t):
      return WIFI_PKT_MGMT;

      // Data packet
      default:
      return WIFI_PKT_DATA;
    }
}

// In this example, the packet handler function does all the parsing and output work.
// This is NOT ideal.
void wifi_sniffer_packet_handler(uint8_t *buff, uint16_t len)
{
  // First layer: type cast the received buffer into our generic SDK structure
  const wifi_promiscuous_pkt_t *ppkt = (wifi_promiscuous_pkt_t *)buff;
  // Second layer: define pointer to where the actual 802.11 packet is within the structure
  const wifi_ieee80211_packet_t *ipkt = (wifi_ieee80211_packet_t *)ppkt->payload;
  // Third layer: define pointers to the 802.11 packet header and payload
  const wifi_ieee80211_mac_hdr_t *hdr = &ipkt->hdr;
  const uint8_t *data = ipkt->payload;

  // Pointer to the frame control section within the packet header
  const wifi_header_frame_control_t *frame_ctrl = (wifi_header_frame_control_t *)&hdr->frame_ctrl;

  // Parse MAC addresses contained in packet header into human-readable strings
  char addr1[] = "00:00:00:00:00:00\0";
  char addr2[] = "00:00:00:00:00:00\0";
  char addr3[] = "00:00:00:00:00:00\0";

  mac2str(hdr->addr1, addr1);
  mac2str(hdr->addr2, addr2);
  mac2str(hdr->addr3, addr3);

if(frame_ctrl->subtype == PROBE_REQ) {
    // Output info to serial

  StaticJsonDocument<500> client;
    client["mac1"] = addr1;
    client["mac2"] = addr2;
    client["mac3"] = addr3;
    client["seq"] = hdr->sequence_ctrl;
    client["rssi"] = ppkt->rx_ctrl.rssi;
    client["channel"] = wifi_get_channel();
    client["protocol"] = frame_ctrl->protocol;
    client["type"] = frame_ctrl->type;
    client["subtype"] = frame_ctrl->subtype;
    client["to_ds"] = frame_ctrl->to_ds;
    client["from_ds"] = frame_ctrl->from_ds;
    
    client["more_frag"] = frame_ctrl->more_frag;
    client["retry"] = frame_ctrl->retry;
    client["pwr_mgmt"] = frame_ctrl->pwr_mgmt;

    client["more_data"] = frame_ctrl->more_data;
    client["wep"] = frame_ctrl->wep;
    client["strict"] = frame_ctrl->strict;

  serializeJson(client, Serial);
  Serial.println();
  Serial.flush();

}



  // Print ESSID if beacon
//   if (frame_ctrl->type == WIFI_PKT_MGMT && frame_ctrl->subtype == BEACON)
//   {
//     const wifi_mgmt_beacon_t *beacon_frame = (wifi_mgmt_beacon_t*) ipkt->payload;
//     char ssid[32] = {0};

//     if (beacon_frame->tag_length >= 32)
//     {
//       strncpy(ssid, beacon_frame->ssid, 31);
//     }
//     else
//     {
//       strncpy(ssid, beacon_frame->ssid, beacon_frame->tag_length);
//     }

//     Serial.printf("%s", ssid);
//   }
}

int channel_hop_interval = 2000;
int last_hop_millis = 0;
int current_channel = 1;

void channel_hop() {
  int current_hop_millis = millis();
  if(current_hop_millis - last_hop_millis >= channel_hop_interval) {
    last_hop_millis = current_hop_millis;
    if(current_channel == 13) {
      current_channel = 1;
    } else {
      current_channel += 1;
    }
  }
  wifi_set_channel(current_channel);
}

void toggleLED() {
    digitalWrite(LED_BUILTIN, LOW);
    delay(20);
    digitalWrite(LED_BUILTIN, HIGH);
}

void startLEDBlinking() {
  unsigned long currnetMillis = millis();

  if(currnetMillis - snotBlinkMillis >= snotBlinkInterval) {
    snotBlinkMillis = currnetMillis;
    toggleLED();
  }
}

void setup()
{
  // Serial setup
  Serial.begin(115200);
  delay(10);
  wifi_set_channel(current_channel);

  // Wifi setup
  wifi_set_opmode(STATION_MODE);
  wifi_promiscuous_enable(0);
  WiFi.disconnect();

  // Set sniffer callback
  wifi_set_promiscuous_rx_cb(wifi_sniffer_packet_handler);
  wifi_promiscuous_enable(1);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  // Print header
//  Serial.printf("\n\n     MAC Address 1|      MAC Address 2|      MAC Address 3| Ch| RSSI| Pr| T(S)  |           Frame type         |TDS|FDS| MF|RTR|PWR| MD|ENC|STR|   SSID");

}

void loop()
{
 startLEDBlinking();
 channel_hop();
 delay(10);
}
